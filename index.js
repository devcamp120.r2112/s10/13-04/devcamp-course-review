// import express from "express";
const express = require("express");
const mongoose = require("mongoose");

const courseRouter = require("./app/routes/courseRouter");
const reviewRouter = require("./app/routes/reviewRouter");

// Khoi tao app express
const app = express();

app.use(express.json());

app.use(express.urlencoded({
    extended: true
}))

const port = 8000;

mongoose.connect("mongodb://localhost:27017/CRUD_Course", (error) => {
    if(error) {
        throw error;
    }

    console.log("Connect successfully!");
})

// Middleware
app.use((request, response, next) => {
    console.log("Time: ", new Date());

    next();
})

app.use((request, response, next) => {
    console.log("Request method: ", request.method);
    
    next();
})
//End Middleware

app.get("/", (request, response) => {
    let today = new Date();

    response.json({
        message: `Xin chào, hôm nay là ngày ${today.getDate()}`
    })
})

//Router level middleware
app.use("/", courseRouter);
app.use("/", reviewRouter);

app.listen(port, () => {
    console.log("App listening on port " + port);
})