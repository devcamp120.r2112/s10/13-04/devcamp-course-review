const mongoose = require("mongoose");
const reviewModel = require("../models/reviewModel");
const courseModel = require("../models/courseModel");

const createReview = (request, response) => {
    let stars = request.body.stars;
    let note = request.body.note;

    let courseId = request.params.courseId;

    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Bad request",
            message: "Course ID is not valid"
        })
    }

    reviewModel.create({
        _id: mongoose.Types.ObjectId(),
        stars: stars,
        note: note
    }, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            courseModel.findByIdAndUpdate(courseId, {
                $push: { reviews:  data._id }
            }, (err, courseUpdated) => {
                if(err) {
                    return response.status(500).json({
                        status: "Internal server error",
                        message: err.message
                    })
                } else {
                    return response.status(201).json({
                        status: "Created",
                        data: data
                    })
                }
            })
        }
    })
}

const getAllReview = (request, response) => {
    var { star } = request.query;

    let condition = {}

    if(star !== undefined && !Array.isArray(star)) {
        star = [star]
    }

    if(Array.isArray(star)) {
        condition.stars = {
            $in: star
        }
    }

    reviewModel.find(condition, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success",
                data: data
            })
        }
    })
}

const getAllReviewOfCourse = (request, response) => {
    let courseId = request.params.courseId;

    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Bad request",
            message: "Course Id is not valid"
        })
    }

    courseModel.findById(courseId)
        .populate("reviews")
        .exec((error, data) => {
            if(error) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Success",
                    data: data.reviews
                })
            }
        })
}

const getReviewById = (request, response) => {
    let reviewId = request.params.reviewId;

    if(!mongoose.Types.ObjectId.isValid(reviewId)) {
        return response.status(400).json({
            status: "Bad request",
            message: "Review Id is not valid"
        })
    }

    reviewModel.findById(reviewId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            if(data) {
                return response.status(200).json({
                    status: "Success",
                    data: data
                })
            } else {
                return response.status(404).json({
                    status: "Not found"
                })
            }
           
        }
    })
}

const updateReviewById = (request, response) => {
    let stars = request.body.stars;
    let note = request.body.note;

    let reviewId = request.params.reviewId;

    if(!mongoose.Types.ObjectId.isValid(reviewId)) {
        return response.status(400).json({
            status: "Bad request",
            message: "Review Id is not valid"
        })
    }

    reviewModel.findByIdAndUpdate(reviewId, {
        stars: stars,
        note: note
    }, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            if(data) {
                return response.status(200).json({
                    status: "Success",
                    data: data
                })
            } else {
                return response.status(404).json({
                    status: "Not found"
                })
            }
        }
    })
}

const deleteReviewById = (request, response) => {
    let reviewId = request.params.reviewId;
    let courseId = request.params.courseId;

    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Bad request",
            message: "Course Id is not valid"
        })
    }

    if(!mongoose.Types.ObjectId.isValid(reviewId)) {
        return response.status(400).json({
            status: "Bad request",
            message: "Review Id is not valid"
        })
    }

    reviewModel.findByIdAndDelete(reviewId, (error) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            courseModel.findByIdAndUpdate(courseId, {
                $pull: { reviews: reviewId }
            }, (err) => {
                if(err) {
                    return response.status(500).json({
                        status: "Internal server error",
                        message: err.message
                    })
                } else {
                    return response.status(204).json()
                }
            })
        }
    })
}

module.exports = {
    createReview,
    getAllReview,
    getAllReviewOfCourse,
    getReviewById,
    updateReviewById,
    deleteReviewById
}

