const mongoose = require("mongoose");
const courseModel = require("../models/courseModel");

const createCourse = (request, response) => {
    let title = request.body.title;
    let description = request.body.description;
    let student = request.body.sudent;

    if(!title) {
        return response.status(400).json({
            status: "Bad request",
            message: "Title is required"
        })
    }

    courseModel.create({
        _id: mongoose.Types.ObjectId(),
        title: title,
        description: description,
        noStudent: student
    }, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Created",
                data: data
            })
        }
    })
}

const getAllCourse = (request, response) => {
    const { title, minStudent, maxStudent } = request.query;

    const condition = {}

    if(title) {
        const regex = new RegExp(`${title}`)

        condition.title = regex
    }

    if(minStudent) {
        condition.noStudent = {
            ...condition.noStudent,
            $gte: minStudent
        }
    }

    if(maxStudent) {
        condition.noStudent = {
            ...condition.noStudent,
            $lte: maxStudent
        }
    }

    courseModel.find(condition, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success",
                data: data
            })
        }
    })
}

const getCourseById = (request, response) => {
    let courseId = request.params.courseId;

    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Bad request",
            message: "Course Id is not valid"
        })
    }

    courseModel.findById(courseId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            if(data) {
                return response.status(200).json({
                    status: "Success",
                    data: data
                })
            } else {
                return response.status(404).json({
                    status: "Not found"
                })
            }
           
        }
    })
}

const updateCourseById = (request, response) => {
    let title = request.body.title;
    let description = request.body.description;
    let student = request.body.sudent;

    let courseId = request.params.courseId;

    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Bad request",
            message: "Course Id is not valid"
        })
    }

    courseModel.findByIdAndUpdate(courseId, {
        title: title,
        description: description,
        noStudent: student
    }, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            if(data) {
                return response.status(200).json({
                    status: "Success",
                    data: data
                })
            } else {
                return response.status(404).json({
                    status: "Not found"
                })
            }
        }
    })
}

const deleteCourseById = (request, response) => {
    let courseId = request.params.courseId;

    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Bad request",
            message: "Course Id is not valid"
        })
    }

    courseModel.findByIdAndDelete(courseId, (error) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            return response.status(204).json()
        }
    })
}

module.exports = {
    createCourse,
    getAllCourse,
    getCourseById,
    updateCourseById,
    deleteCourseById
}

