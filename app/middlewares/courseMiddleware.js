const printCourseUrlMiddleware = (request, response, next) => {
    console.log("Request Course URL: ", request.url);

    next();
}

module.exports = { printCourseUrlMiddleware }