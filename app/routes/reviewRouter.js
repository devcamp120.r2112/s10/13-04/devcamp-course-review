const express = require("express");

const { printReviewUrlMiddleware } = require("../middlewares/reviewMiddleware");

const { createReview, getAllReview, getAllReviewOfCourse, getReviewById, updateReviewById, deleteReviewById } = require("../controllers/reviewController");

const router = express.Router();

router.get("/reviews", printReviewUrlMiddleware, getAllReview);

router.post("/courses/:courseId/reviews", printReviewUrlMiddleware, createReview);

router.get("/courses/:courseId/reviews", printReviewUrlMiddleware, getAllReviewOfCourse);

router.get("/reviews/:reviewId", printReviewUrlMiddleware, getReviewById);

router.put("/reviews/:reviewId", printReviewUrlMiddleware, updateReviewById);

router.delete("/courses/:courseId/reviews/:reviewId", printReviewUrlMiddleware, deleteReviewById);

// export router;
module.exports = router;