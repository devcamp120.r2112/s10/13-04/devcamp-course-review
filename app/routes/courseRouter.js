const express = require("express");

const { printCourseUrlMiddleware } = require("../middlewares/courseMiddleware");

const { createCourse, getAllCourse, getCourseById, updateCourseById, deleteCourseById } = require("../controllers/courseController");

const router = express.Router();

router.get("/courses", printCourseUrlMiddleware, getAllCourse);

router.post("/courses", printCourseUrlMiddleware, createCourse);

router.get("/courses/:courseId", printCourseUrlMiddleware, getCourseById)

router.put("/courses/:courseId", printCourseUrlMiddleware, updateCourseById)

router.delete("/courses/:courseId", printCourseUrlMiddleware, deleteCourseById)

// export router;
module.exports = router;