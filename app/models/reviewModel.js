const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const reviewSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    stars: {
        type: Number,
        default: 0
    },
    note: {
        type: String,
        required: false
    }
});

module.exports = mongoose.model("review", reviewSchema);